package com.mythictable.config

object Config {
  val serverBaseURL = "http://mythicapp"
  val atOnceUsers = 1
}
