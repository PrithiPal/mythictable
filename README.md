# Mythic Table

**Welcome to Mythic Table!<br/>A virtual tabletop application for playing games with your friends online.<br/>If this is your first time here, read more on our website: [https://mythictable.com/](https://mythictable.com/)**

# Quick Start 

If the following doesn't make sense, please contact someone in our [Slack](https://mythictable.slack.com). We'll help you out the best we can! <br/>Many who find this page first will be software developers so we made some assumptions.

## First time setup
1. [Install .NET Core SDK, version 3.1 or higher](https://dotnet.microsoft.com/download) (backend)
2. Install node.js using Visual Studio Installer or [nodejs.org](https://nodejs.org/en/download/) (frontend)
3. Start the MythicAuth service 
   1. Clone the mythicauth repository (`git clone https://gitlab.com/mythicteam/mythicauth.git`)
   2. Run `dotnet run` or `dotnet watch run .` within `mythicauth/src/MythicAuth`
      - `dotnet watch run .` recompiles the service when a file is changed
      - `dotnet run` just runs the service and does not monitor file changes
4. Clone the main repository (`git clone https://gitlab.com/mythicteam/mythictable.git`)
5. Start MythicTable's Backend
    1. Run `dotnet dev-certs https` and `dotnet dev-certs https --trust` from within `mythictable/server/src/MythicTable`.
    2. Run `dotnet run` or `dotnet watch run .` within `mythictable/server/src/MythicTable`
6. Start MythicTable's Frontend
    1. Install dependencies using `npm install` and `npm install vue-cli` from within `mythictable/html`
    2. Run `npm run serve` within `mythictable/html`
7. Enjoy!
8. If you use Powershell, you could skip steps 5-6 by running `Start-DevEnv.ps1` within the main `mythictable` folder
    - If this is the first time running MythicTable, use `Start-DevEnv.ps1 -isFirstTime`

# Working with MongoDB

The above steps will be enough to get you up and going with an in-memory
database, but in some cases, that's not good enough. This is especially
true if you find you're restarting the server a lot in the process of writing
code or troubleshooting. In cases like this, it might be better to run the
services with a local instance of MongoDB. There are a number of ways to 
do this, but this is the Mythic Table recommended approach:

## Install Docker

Docker is a container system that allows applications to be run in an enviroment
independant of your operating system. Mythic Table uses Docker for a number
of things including our production environment, but we've been shying away from
in for local development environments. The reason is that we want to keep
things as simple as possible and though Docker is not complicated, it does
remain a possible point of failure. However, do not be disheartened, it is
extremely easy to set up and use. Especially with a set of commands to copy
and paste. ;)

### Windows and Mac users

[Docker Desktop](https://www.docker.com/products/docker-desktop) is a great tool
to get started with. Just go [here](https://www.docker.com/products/docker-desktop)
and follow the instructions.

### Linux users

This is much easier for Linux users.
* [Centos](https://docs.docker.com/install/linux/docker-ce/centos/)
* [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
* [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
* [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

This is an optional step, but highly recommended:
* [Enable Docker CLI for the non-root user](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
account that will be used to run VS Code.

### VS Code Users

* Get the [Docker Extension](https://github.com/microsoft/vscode-docker)
* You can also just search for it under extensions.

## Docker Registry

Docker uses a public image registry to host common images. You will likely need to make an account [here](https://hub.docker.com/)

Then you will login using:
* `docker login`

## The MongoDB Image

* `docker pull mongo`
* `docker run -d --rm -p 27017-27019:27017-27019 --name mongodb mongo:latest`

What do these commands do?

* `docker` is the CLI tool for docker.  You can `docker --help` for more information
* `pull` will pull the image from Docker Hub
* `run` will run the image
* `-d` specifies daemon mode so the image is run in the background
* `--rm` is a nice feature to remove the image after it's terminated
* `-p 27017-27019:27017-27019` instructs docker how to listen and forward ports
* `--name mongodb` creates a user friendly name
* `mongo:latest` the last argument is the image name and tag

You may wish to stop and start later the mongodb server (or docker itself). You can stop and start the mongodb image by doing the following:
* `docker stop mongo`
* `docker start mongo`

## Setting up MongoDB

MongoDB requires a specific user with a specific password for local development.
These steps will walk you through the process of setting that up.

* `docker exec -it mongo mongo`

What's happenging?
* `exec` execute something in the docker container
* `-it` interactively
* `mongo` the first mongo is your image name
* `mongo` the second is your command to run.

You should now have a mongo prompt
```
>
```

From here run the following:

```
use admin
db.createUser(
  {
        user: "admin",
        pwd: "abc123!",
        roles: [ { role: "root", db: "admin" } ]
  }
);
exit;
```

### Note

If the admin user isn't added, you might get the following error:
```
fail: MythicTable.Middleware.ErrorHandlerMiddleware[0]
      Unhandled Exception: Unable to authenticate using sasl protocol mechanism SCRAM-SHA-1.
```

## Running the server to connect to MongoDB

By default the server uses an in-memory collection for data persistance. To
get it to use mongo, add `--launch-profile local-mongo`. This will instruct it to 
use the right profile.
* `dotnet run --project .\server\src\MythicTable\MythicTable.csproj --launch-profile local-mongo`

or

* `cd server/src/MythicTable/`
* `dotnet run --launch-profile local-mongo`
