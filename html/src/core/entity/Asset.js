import Vue from 'vue';

class Asset {
    static loadAll(entities) {
        const results = [];
        for (const entity of Object.values(entities)) {
            const asset = entity.asset;
            if (!asset) continue;

            if (asset['@data']) {
                return Promise.resolve(entity);
            }

            Vue.set(asset, '@data', null);

            if (asset.kind === 'image' && asset.src) {
                const image = new Image();
                const promise = new Promise((resolve, reject) => {
                    image.addEventListener(
                        'load',
                        () => {
                            asset['@data'] = image;
                            resolve(entity);
                        },
                        { once: true },
                    );
                    image.addEventListener(
                        'error',
                        ev => {
                            // TODO: Better error handling
                            reject(ev);
                        },
                        { once: true },
                    );

                    image.src = asset.src;
                });

                results.push(promise);
            }
        }

        return results;
    }
}

export { Asset as default };
