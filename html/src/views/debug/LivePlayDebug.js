import Asset from '@/core/entity/Asset';

async function loadDebugScene({ vm, sessionId, sceneId }) {
    if (sceneId == 'debug') {
        const data = await import('./data_demo');
        await data.loader(vm.$store);

        Asset.loadAll(vm.$store.state.gamestate.entities);

        vm.director.sessionId = sessionId;
        vm.director.connect();
    }
}

export { loadDebugScene };
