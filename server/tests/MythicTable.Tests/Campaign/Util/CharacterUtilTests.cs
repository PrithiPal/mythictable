using System;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using Xunit;

namespace MythicTable.Tests.Campaign.Util
{
    public class CharacterUtilTests
    {
        [Fact]
        public void TestCreateCharacterWithImage()
        {
            var character = CharacterUtil.CreateCharacter("/static/assets/marc.png", 7, 18);
            Assert.Equal("/static/assets/marc.png", character.Asset["src"]);
            Assert.Equal("image", character.Asset["kind"]);
            Assert.Equal(7, character.Token["pos"]["q"]);
            Assert.Equal(18, character.Token["pos"]["r"]);
        }
        
        [Fact]
        public void TestCreateCharacterWithColor()
        {
            var character = CharacterUtil.CreateColorToken("red", 31, 2);
            Assert.Equal("red", character.Token["image"]["color"]);
            Assert.Equal(31, character.Token["pos"]["q"]);
            Assert.Equal(2, character.Token["pos"]["r"]);
        }
    }
}