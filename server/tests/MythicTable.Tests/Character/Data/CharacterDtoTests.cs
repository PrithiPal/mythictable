using MongoDB.Bson;
using MythicTable.Campaign.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Tests.Character.Data
{
    public class CharacterDtoTests
    {
        [Fact]
        public void TestCharacterSerialization()
        {
            var character = new CharacterDTO();
            character.Token = BsonDocument.Parse("{pos: {r: 18}}");
            var str = JsonConvert.SerializeObject(character);
            var json = JsonConvert.DeserializeObject<JObject>(str);
            Assert.Equal(18.0, json["token"]["pos"]["r"]);
        }
    }
}