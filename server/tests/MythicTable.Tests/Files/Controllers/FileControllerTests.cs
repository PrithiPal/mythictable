using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MythicTable.Files.Controllers;
using MythicTable.Files.Data;
using MythicTable.Files.Store;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Tests.Files.Controllers
{
    public class FileControllerTests
    {
        private FileController controller;
        private Mock<IFileOwnershipProvider> mockProvider;
        private Mock<IFileStore> mockStore;

        private string User { get; set; } = "TestUser";

        public FileControllerTests()
        {
            mockProvider = new Mock<IFileOwnershipProvider>();
            mockStore = new Mock<IFileStore>();

            controller = new FileController(mockProvider.Object, mockStore.Object);
            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => {
                               return new Claim("", User);
                           });
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
        }

        [Fact]
        public async Task TestGetsEmptyList()
        {
            mockProvider.Setup(provider => provider.GetAll(It.IsAny<string>()))
                .ReturnsAsync(new List<FileDto>());

            var actionResult = await controller.GetFiles() as ActionResult<List<FileDto>>;
            var files = actionResult.Value as List<FileDto>;
            Assert.Empty(files);
        }

        [Fact]
        public async Task TestGetFiles()
        {
            List<FileDto> mockFiles = new List<FileDto>
            {
                new FileDto(),
                new FileDto(),
            };
            mockProvider.Setup(provider => provider.GetAll(It.IsAny<string>()))
                .ReturnsAsync(mockFiles);

            var actionResult = await controller.GetFiles() as ActionResult<List<FileDto>>;
            var files = actionResult.Value as List<FileDto>;
            Assert.Equal(2, files.Count);
        }

        [Fact]
        public async Task TestGetSingleFile()
        {
            const string Id = "file-id";
            mockProvider.Setup(provider => provider.Get(Id, User))
                .ReturnsAsync(new FileDto { Id = Id });

            var actionResult = await controller.GetFile(Id) as ActionResult<FileDto>;
            var file = actionResult.Value as FileDto;
            Assert.Equal(Id, file.Id);
        }

        [Fact]
        public async Task TestPostFile()
        {
            mockProvider.Setup(provider => provider.Store(It.IsAny<FileDto>()))
                .ReturnsAsync((FileDto dto) => dto);
            mockStore.Setup(store => store.SaveFile(It.IsAny<IFormFile>()))
                .ReturnsAsync(new FileDto { Path = "file-path" });

            IFormFile file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("blank")), 0, 5, "Data", "dummy.txt");

            var actionResult = await controller.PostFile(new List<IFormFile> { file }) as ActionResult<UploadResult>;
            var okResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var results = Assert.IsType<UploadResult>(okResult.Value);
            Assert.Equal(1, results.Count);
            Assert.Single(results.Files);
            Assert.Equal("file-path", results.Files.Single().Path);

            mockStore.Verify(store => store.SaveFile(It.Is<IFormFile>(
                form => form.Name == "Data" && form.FileName == "dummy.txt")));
            mockProvider.Verify(provider => provider.Store(
                It.Is<FileDto>( f => f.Path == "file-path" && f.User == User)
            ));
        }

        [Fact]
        public async Task TestDeleteFile()
        {
            const string Id = "file-id";
            mockProvider.Setup(provider => provider.Get(Id, User))
                .ReturnsAsync(new FileDto { Id = Id, Path = "file-path" });
            mockProvider.Setup(provider => provider.Delete(Id, User))
                .ReturnsAsync(new FileDto { Id = Id });
            mockStore.Setup(store => store.DeleteFiles(It.IsAny<List<FileDto>>()));

            var actionResult = await controller.DeleteFile(new List<string>{ Id }) as ActionResult<DeleteResult>;
            var okResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var results = Assert.IsType<DeleteResult>(okResult.Value);
            Assert.Equal(1, results.Count);
            Assert.Single(results.Paths);
            Assert.Equal("file-path", results.Paths.Single());

            mockStore.Verify(store => store.DeleteFiles(It.Is<List<FileDto>>(
                files => files.Single().Path == "file-path")));
        }
    }
}
