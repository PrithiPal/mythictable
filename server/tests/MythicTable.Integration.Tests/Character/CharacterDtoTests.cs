using System;
using Mongo2Go;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using MythicTable.Integration.Tests.Util;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using MythicTable.Campaign.Data;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MythicTable.Integration.Tests.Character
{
    public class CharacterDtoTests : IDisposable
    {
        private MongoDbRunner runner;
        private HttpClient client;

        public CharacterDtoTests()
        {
            runner = MongoDbRunner.Start();
            Environment.SetEnvironmentVariable("MTT_MONGODB_CONNECTIONSTRING", runner.ConnectionString);
            Environment.SetEnvironmentVariable("MTT_MONGODB_DATABASENAME", "mythictable");
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            client = server.CreateClient();
        }

        public void Dispose()
        {
            client.Dispose();
            runner.Dispose();
        }

        [Fact]
        public async Task GetsCharactersTests()
        {
            string campaignId = await CreationCampaign("Integration Test Campaign");
            var charactersJson = await GetCharacters(campaignId);
            Assert.Equal(28, charactersJson.Count);
            Assert.Equal("/static/assets/marc.png", (string)charactersJson[0]["asset"]["src"]);
            Assert.Equal(7, (int)charactersJson[0]["token"]["pos"]["q"]);
            Assert.Equal(18, (int)charactersJson[0]["token"]["pos"]["r"]);
        }

        private async Task<string> CreationCampaign(string campaignName)
        {
            var campaign = new CampaignDTO()
            {
                Name = campaignName
            };
            var cancellationToken = new CancellationToken();
            using (var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign, cancellationToken))
            {
                response.EnsureSuccessStatusCode();
                Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
                var json = await response.Content.ReadAsStringAsync();
                var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
                Assert.Equal(campaignName, newCampaign.Name);
                return newCampaign.Id;
            }
        }

        private async Task<JArray> GetCharacters(string campaignId)
        {
            var cancellationToken = new CancellationToken();
            using (var response = await RequestHelper.GetStreamAsync(client, $"/api/campaigns/{campaignId}/characters", cancellationToken))
            {
                response.EnsureSuccessStatusCode();
                Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
                var json = await response.Content.ReadAsStringAsync();
                return JArray.Parse(json);
            }
        }
    }
}