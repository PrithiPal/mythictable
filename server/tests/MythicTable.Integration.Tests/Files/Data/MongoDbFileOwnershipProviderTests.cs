using Mongo2Go;
using MongoDB.Driver;
using MythicTable.Files.Data;
using MythicTable.Files.Exceptions;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Integration.Tests.Files.Data
{
    public class MongoDbFileOwnershipProviderTests: IAsyncLifetime
    {
        private MongoDbRunner runner;
        public IFileOwnershipProvider provider;

        public MongoDbFileOwnershipProviderTests()
        {
            provider = new InMemoryFileOwnershipProvider();
        }

        public Task InitializeAsync()
        {
            runner = MongoDbRunner.Start();
            var settings = new MongoDbSettings
            {
                ConnectionString = runner.ConnectionString,
                DatabaseName = "mythictable"
            };
            var client = new MongoClient(settings.ConnectionString);
            provider = new MongoDbFileOwnershipProvider(settings, client);
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            runner.Dispose();
            return Task.CompletedTask;
        }

        [Fact]
        public async Task TestCreateAndGet()
        {
            var createdDto = await provider.Create("file", "user");
            var fetchedDto = await provider.Get(createdDto.Id, "user");
            Assert.Equal(createdDto.Id, fetchedDto.Id);
            Assert.Equal(createdDto.Path, fetchedDto.Path);
        }

        [Fact]
        public async Task TestGetNonExistentThrows()
        {
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get("5515836e58c7b4fbc756320b", "user"));
            Assert.Equal("Could not find File of Id: '5515836e58c7b4fbc756320b'", exception.Message);
        }

        [Fact]
        public async Task TestGetWithWrongUserThrows()
        {
            var createdDto = await provider.Create("file", "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get(createdDto.Id, "user2"));
            Assert.Equal($"File '{createdDto.Id}' does not belong to user 'user2'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteNonExistentThrows()
        {
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Delete("5515836e58c7b4fbc756320b", "user"));
            Assert.Equal("Could not find File of Id: '5515836e58c7b4fbc756320b'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteWithWrongUserThrows()
        {
            var createdDto = await provider.Create("file", "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Delete(createdDto.Id, "user2"));
            Assert.Equal($"File '{createdDto.Id}' does not belong to user 'user2'", exception.Message);
        }

        [Fact]
        public async Task TestDeleteRemovesTheFile()
        {
            var createdDto = await provider.Create("file", "user");
            await provider.Delete(createdDto.Id, "user");
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get(createdDto.Id, "user"));
            Assert.Equal($"Could not find File of Id: '{createdDto.Id}'", exception.Message);
        }

        [Fact]
        public async Task TestCreateAndGetAll()
        {
            await provider.Create("file1", "user");
            await provider.Create("file2", "user");
            var all = await provider.GetAll("user");
            Assert.Equal(2, all.Count);
        }

        [Fact]
        public async Task TestStoreAddsId()
        {
            var dto = new FileDto
            {
                Path = "",
                User = "user"
            };
            Assert.Null(dto.Id);
            dto = await provider.Store(dto);
            Assert.NotNull(dto.Id);
        }

        [Fact]
        public async Task TestThrowsCannotFindWhenIdIsNotObjectId()
        {
            var exception = await Assert.ThrowsAsync<FileStorageException>(
                () => provider.Get("1", "user"));
            Assert.Equal($"Could not parse Id: '1'", exception.Message);
        }
    }
}
