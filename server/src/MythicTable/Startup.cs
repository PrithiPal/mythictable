﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.OpenApi.Models;
using NodaTime;
using NodaTime.Serialization.JsonNet;
using MongoDB.Driver;
using Mongo.Migration.Startup.DotNetCore;
using MythicTable.GameSession;
using MythicTable.Campaign.Data;
using MythicTable.Middleware;
using Microsoft.IdentityModel.Logging;
using MythicTable.Files.Data;
using MythicTable.Files.Store;

namespace MythicTable
{
    public class Startup
    {
        private ILogger Logger;
        private Microsoft.AspNetCore.Hosting.IWebHostEnvironment Env;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb);
            }).AddApiExplorer();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            services.AddCors();
            services.AddSignalR().AddNewtonsoftJsonProtocol();
            services.AddRazorPages();

            services.AddSingleton<IEntityCollection>(new EntityCollection());
            services.AddSingleton<IGameState>(new GameState());

            addDatabase(services);
            addFileStorage(services);

            services.AddControllers();

            ConfigureAuthentication(services);
            services.AddAuthorization();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IWebHostEnvironment env, ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Env = env;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Make sure CORS policy accepts client running with node.js
                var allowedOrigin = Environment.GetEnvironmentVariable("MTT_ALLOW_ORIGIN");
                if (allowedOrigin != null)
                {
                    app.UseCors(builder =>
                    {
                        builder.WithOrigins(allowedOrigin)
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
                }
                
                IdentityModelEventSource.ShowPII = true; 
            }
            
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mythic Table API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<LivePlayHub>("/api/live");
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });

            {
                IFileProvider staticFileProvider = new PhysicalFileProvider(
                    Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot"));

                // For development, try serving files from client directory first
                if (env.IsDevelopment())
                {
                    var clientOutputDir = new DirectoryInfo("../../../html/dist/");
                    if (clientOutputDir.Exists)
                    {
                        staticFileProvider = new CompositeFileProvider(
                            new PhysicalFileProvider(clientOutputDir.FullName),
                            staticFileProvider);

                        this.Logger.LogInformation("Serving client files from '{0}'.",
                            clientOutputDir.FullName);
                    }
                    else
                    {
                        this.Logger.LogWarning(
                            "Client build output directory not found at '{0}'. Client assets will not be served.",
                            clientOutputDir.FullName);
                    }
                }

                app.Use(async (context, next) =>
                {
                    // Redirect all unknown paths to default file so that client-side SPA works
                    var requestPath = context.Request.Path;
                    var isAllowedStaticFile = requestPath.StartsWithSegments("/css")
                        || requestPath.StartsWithSegments("/js")
                        || requestPath.StartsWithSegments("/static")
                        || requestPath.StartsWithSegments("/.well-known")
                        || requestPath == "/favicon.ico";

                    if (!isAllowedStaticFile)
                    {
                        context.Request.Path = new PathString("/index.html");
                    }

                    await next();
                });

                app.UseStaticFiles(
                    new StaticFileOptions { FileProvider = staticFileProvider });
            }
        }

        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                // Identity made Cookie authentication the default.
                // However, we want JWT Bearer Auth to be the default.
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Authority = Env.IsProduction()
                    ? "https://auth.edge.mythictable.com"
                    : "http://localhost:5002";
                options.RequireHttpsMetadata = Env.IsProduction();

                options.Audience = "mythictableserver"; // matched ApiResource in MythicIdentityServer

                // We have to hook the OnMessageReceived event in order to
                // allow the JWT authentication handler to read the access
                // token from the query string when a WebSocket or
                // Server-Sent Events request comes in.

                // Sending the access token in the query string is required due to
                // a limitation in Browser APIs. We restrict it to only calls to the
                // SignalR hub in this code.
                // See https://docs.microsoft.com/aspnet/core/signalr/security#access-token-logging
                // for more information about security considerations when using
                // the query string to transmit the access token.
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        // If the request is for our hub...
                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) &&
                            (path.StartsWithSegments("/api")))
                        {
                            // Read the token out of the query string
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = context =>
                    {
                        var authException = context.Exception;
                        Logger.LogDebug(authException.ToString());
                        return Task.CompletedTask;
                    }
                };
            });
        }

        private void addDatabase(IServiceCollection services)
        {
            var conn = Environment.GetEnvironmentVariable("MTT_MONGODB_CONNECTIONSTRING");
            var db = Environment.GetEnvironmentVariable("MTT_MONGODB_DATABASENAME");
            if(string.IsNullOrEmpty(conn) || string.IsNullOrEmpty(db))
            {
                Console.Out.WriteLine("Using an in memory datastore");
                services.AddSingleton<ICampaignProvider>(new InMemoryCampaignProvider());
                services.AddSingleton<IFileOwnershipProvider, InMemoryFileOwnershipProvider>();
            }
            else
            {
                var host = conn.Substring(conn.LastIndexOf('@') + 1);
                Console.Out.WriteLine($"Using a mongodb datastore at {host} with db name {db}");
                var mongoSettings = new MongoDbSettings 
                {
                    ConnectionString = conn,
                    DatabaseName = db
                };
                services.AddSingleton<MongoDbSettings>(mongoSettings);
                services.AddSingleton<IMongoClient>(new MongoClient(mongoSettings.ConnectionString));
                services.AddSingleton<ICampaignProvider, MongoDbCampaignProvider>();
                services.AddSingleton<IFileOwnershipProvider, MongoDbFileOwnershipProvider>();
                services.AddMigration();
            }
        }

        private void addFileStorage(IServiceCollection services)
        {
            if (Configuration.GetValue<Boolean>("MTT_USE_GCP_IMAGE_STORE"))
            {
                services.AddSingleton<IFileStore, GoogleCloudStore>();
            }
            else
            {
                var projectRoot = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("server"));
                var assetPath = Path.GetFullPath(Path.Join(projectRoot, "html/public/static/assets/user-files/"));
                services.AddSingleton<IFileStore>(new LocalFileStore(assetPath, new FileWriter(), "http://localhost:5000/static/assets/user-files/"));
            }
        }
    }
}