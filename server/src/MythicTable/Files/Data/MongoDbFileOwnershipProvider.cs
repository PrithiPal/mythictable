﻿using MongoDB.Bson;
using MongoDB.Driver;
using MythicTable.Files.Exceptions;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace MythicTable.Files.Data
{
    public class MongoDbFileOwnershipProvider : IFileOwnershipProvider
    {
        private readonly IMongoCollection<FileDto> files;

        public MongoDbFileOwnershipProvider(MongoDbSettings settings, IMongoClient client)
        {
            var database = client.GetDatabase(settings.DatabaseName);
            files = database.GetCollection<FileDto>("files");
        }

        public Task<FileDto> Create(string path, string userId)
        {
            var dto = new FileDto
            {
                Path = path,
                User = userId
            };
            return Store(dto);
        }

        public async Task<FileDto> Delete(string id, string userId)
        {
            var dto = await Get(id, userId);
            var filter = Builders<FileDto>.Filter.Eq("Id", id);
            await files.DeleteOneAsync(filter);
            return dto;
        }

        public async Task<FileDto> Get(string id, string userId)
        {
            var filter = Builders<FileDto>.Filter.Eq("Id", MakeObjectId(id));
            var dto = await files.Find(filter).FirstOrDefaultAsync();
            if (dto == null)
            {
                throw new FileStorageException($"Could not find File of Id: '{id}'", HttpStatusCode.NotFound);
            }
            if (dto.User != userId)
            {
                throw new FileStorageException($"File '{id}' does not belong to user '{userId}'", HttpStatusCode.Forbidden);
            }
            return dto;
        }

        public async Task<List<FileDto>> GetAll(string userId)
        {
            var filter = Builders<FileDto>.Filter.Eq("user", userId);
            return await files.Find(filter).ToListAsync();
        }

        public async Task<FileDto> Store(FileDto dto)
        {
            await files.InsertOneAsync(dto);
            return dto;
        }

        private static ObjectId MakeObjectId(string id)
        {
            try
            {
                return ObjectId.Parse(id);
            }
            catch
            {
                throw new FileStorageException($"Could not parse Id: '{id}'", HttpStatusCode.NotFound);
            }
        }
    }
}
