﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace MythicTable.Files.Data
{
    public class FileDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("path")]
        [JsonProperty("path")]
        public string Path { get; set; }

        [BsonElement("user")]
        [JsonProperty("user")]
        public string User { get; set; }

        [BsonElement("url")]
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
