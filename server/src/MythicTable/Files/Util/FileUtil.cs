﻿using System;

namespace MythicTable.Files.Util
{
    public class FileUtil
    {
        public static string CreatePath(DateTime date)
        {
            return $"{date:yyyy}/{date:MM}/{date:dd}/";
        }
    }
}
