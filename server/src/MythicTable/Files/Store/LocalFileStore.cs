using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MythicTable.Files.Data;

namespace MythicTable.Files.Store
{
    public class LocalFileStore : IFileStore
    {
        private string path;
        private IFileWriter writer;
        private string urlPrefix;

        public LocalFileStore(string path, IFileWriter writer, string urlPrefix = "/")
        {
            this.path = path;
            this.writer = writer;
            this.urlPrefix = urlPrefix;
        }

        public async Task<FileDto> SaveFile(IFormFile formFile)
        {
            writer.CreateDirectory(this.path);
            string fileName = CreateRandomFileName(formFile.FileName);
            var filePath = Path.Join(this.path, fileName);
            string path = await writer.CopyToFile(formFile, filePath);
            return new FileDto
            {
                Path = filePath,
                Url = urlPrefix + fileName
            };
        }

        public Task DeleteFiles(List<FileDto> files)
        {
            return writer.DeleteFiles(files.Select(f => f.Path).ToList());
        }

        private static string CreateRandomFileName(string orginalFileName)
        {
            var ext = orginalFileName.Substring(orginalFileName.LastIndexOf('.'));
            string randomName = Path.GetRandomFileName();
            return randomName.Substring(0, randomName.LastIndexOf('.')) + ext;
        }
    }
}
