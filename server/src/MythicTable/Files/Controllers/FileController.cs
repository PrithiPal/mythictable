using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MythicTable.Extensions.Controllers;
using MythicTable.Files.Data;
using MythicTable.Files.Store;

namespace MythicTable.Files.Controllers
{
    [Route("api/files")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private IFileOwnershipProvider provider;
        private IFileStore fileStore;

        public FileController(IFileOwnershipProvider provider, IFileStore fileStore)
        {
            this.provider = provider ?? throw new ArgumentNullException(nameof(provider));
            this.fileStore = fileStore ?? throw new ArgumentNullException(nameof(fileStore));
        }

        // GET: api/files
        [HttpGet]
        public async Task<ActionResult<List<FileDto>>> GetFiles()
        {
            return await provider.GetAll(this.GetUserId());
        }

        // GET: api/files/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FileDto>> GetFile(string id)
        {
            return await provider.Get(id, this.GetUserId());
        }

        // POST: api/files
        [HttpPost]
        public async Task<ActionResult<UploadResult>> PostFile(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var user = this.GetUserId();

            var fileDtos = new List<FileDto>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    FileDto dto = await this.fileStore.SaveFile(formFile);
                    dto.User = user;
                    fileDtos.Add(await provider.Store(dto));
                }
            }
            return Ok(new UploadResult { Count = files.Count, Size = size, Files = fileDtos });
        }

        // DELETE: api/file/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DeleteResult>> DeleteFile(List<string> ids)
        {
            var user = this.GetUserId();
            var files = new List<FileDto>();
            foreach (var id in ids)
            {
                var file = await provider.Get(id, user);
                files.Add(file);
                await provider.Delete(id, user);
            }
            await fileStore.DeleteFiles(files);
            return Ok(new DeleteResult { Count = files.Count, Paths = files.Select(f => f.Path).ToList() });
        }

        private static async Task<string> SaveFile(IFormFile formFile)
        {
            var filePath = Path.GetTempFileName();
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }

            return filePath;
        }
    }
}
